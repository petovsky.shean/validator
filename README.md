
### Návrh validátoru pro skeleton

* Řešilo by se vše přes PHP Atributy.
* Každý validátor by měl svůj vlastní Atribut (např. [AddPrefix.php](src%2FValidators%2FAddPrefix.php), [TrimToLength.php](src%2FValidators%2FTrimToLength.php))
* Jednotlivé validátory by se definovali a konfigurovali už na interface: [OrderEntityInterface.php](src%2FEntities%2FOrderEntityInterface.php)
* V adaptéru by se dala zavolat abstraktní metoda `validate()`, která by přes Reflekci našla atributy z interface a provolala na nich všechny validace -- tohle je celkem magic ([AbstractEntity.php](src%2FSkeleton%2FAbstractEntity.php)), na druhou stranu je to pak pěkně čitelné  pro tu konečnou implementaci