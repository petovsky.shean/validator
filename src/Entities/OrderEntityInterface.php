<?php
namespace Shean\Validator\Entities;


use Shean\Validator\Validators\AddPrefix;
use Shean\Validator\Validators\TrimToLength;

interface OrderEntityInterface
{
    #[TrimToLength(3)]
    #[AddPrefix('shean-')]
    public function getDescription(): string;
}