<?php
namespace Shean\Validator\Adapters\ExternalSystem;

use Shean\Validator\Entities\OrderEntityInterface;
use Shean\Validator\Skeleton\AbstractEntity;

class OrderEntity extends AbstractEntity implements OrderEntityInterface
{
    public function __construct(
        private object $externalOrder
    ) {}

    public function getDescription(): string
    {
        return $this->validate(
            __METHOD__,
            $this->externalOrder->description
        );
    }
}