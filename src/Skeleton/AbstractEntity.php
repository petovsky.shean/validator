<?php

namespace Shean\Validator\Skeleton;

class AbstractEntity
{
    protected function validate(string $method, $input)
    {
        $validatedInput = $input;
        $reflectionMethod = new \ReflectionMethod($method);
        foreach ($reflectionMethod->getDeclaringClass()->getInterfaces() as $interface) {
            $interfaceMethod = $interface->getMethod($reflectionMethod->getName());

            foreach ($interfaceMethod?->getAttributes() as $attribute) {
                $validatedInput = ($attribute->newInstance())->validate($validatedInput);
            }
        }
        return $validatedInput;
    }
}