<?php

namespace Shean\Validator\Validators;

#[\Attribute]
class AddPrefix
{
    public function __construct(
        private string $prefix
    ) {}

    public function validate($input)
    {
        return $this->prefix.$input;

    }
}