<?php

namespace Shean\Validator\Validators;

#[\Attribute]
class TrimToLength
{
    public function __construct(
        private int $maxChars
    ) {}

    public function validate($input)
    {
        return mb_substr($input, 0, $this->maxChars);
    }
}